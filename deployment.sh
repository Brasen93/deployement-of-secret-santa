#!/bin/bash

# This script requires minimum Azure CLI version 2.0.74

# Variables
resourceGroupName="krupinska-secret-santa-rg"
appName="krupinska-secret-santa"
location="NorthEurope"
gitrepo=https://github.com/Brasen93/secret-santa-solution.git
container="krupinskasecretsanta"
dockerImageBackend="dkrupinska/secret-santa:latest"
dockerImageFrontend="dkrupinska/secret-santa-ui:latest"

#Clone git repository
echo "Cloning the repo with docker compose"
git clone $gitrepo

#Enter the repository directory
cd secret-santa-solution

# Create a Resource Group
echo "Creating the resource group"
az group create --name $resourceGroupName --location $location

#Create a Azure Docker Registry
echo "Creating the Azure Docker Registry"
az acr create --resource-group $resourceGroupName \
  --name $container --sku Basic

#Log into Azure Docker Registry
echo "Log into the Azure Docker Registry"
az acr login --name $container

echo "Pull, tag and push images"
#Pull Docker image for backend from DockerHub
docker pull $dockerImageBackend

#Tag docker image for backend
docker tag secret-santa-backend $container/secret-santa-backend:v1

#Push docker image for frontend to Azure Docker Registry
docker push $container/secret-santa-frontend:v1

#Pull Docker image for frontend from DockerHub
docker pull $dockerImageFrontend

#Tag docker image for frontend
docker tag secret-santa-frontend $container/secret-santa-frontend:v1

#Push docker image for frontend to Azure Docker Registry
docker push $container/secret-santa-frontend:v1

# Create an App Service Plan
echo "Creating App Service Plan"
az appservice plan create --resource-group $resourceGroupName \
    --name myAppServicePlan --sku S1 --is-linux


#Create WebApp with docker compose
echo "Creating WebApp from docker compose"
az webapp create --resource-group $resourceGroupName --plan myAppServicePlan \
--name $appName --multicontainer-config-type compose --multicontainer-config-file docker-compose.yml

# Copy the result of the following command into a browser to see the web app.
echo http://$appName.azurewebsites.net
